       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TWO-DIM-INIT-TABLE.
       AUTHOR. SARWAUT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 WS-VALUE.
          05 FILLER     PIC X VALUE "A".
          05 FILLER     PIC X VALUE "1".
          05 FILLER     PIC X VALUE "2".
          05 FILLER     PIC X VALUE "B".
          05 FILLER     PIC X VALUE "3".
          05 FILLER     PIC X VALUE "4".
          05 FILLER     PIC X VALUE "C".
          05 FILLER     PIC X VALUE "5".
          05 FILLER     PIC X VALUE "6".
       01 WS-TABLE REDEFINES WS-VALUE.
          05 WS-ROW OCCURS 3 TIMES.
             10 WS-COL  PIC X OCCURS 3 TIMES
                              VALUE "-".
       01 WS-IDX-ROW    PIC 9.
       01 WS-IDX-COL    PIC 9.

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY WS-TABLE 
           PERFORM VARYING WS-IDX-ROW FROM 1 BY 1
              UNTIL WS-IDX-ROW > 3
                   PERFORM VARYING WS-IDX-COL FROM 1 BY 1
                      UNTIL WS-IDX-COL > 3
                           DISPLAY WS-COL(WS-IDX-ROW, WS-IDX-COL) " "
                              WITH NO ADVANCING 
                   END-PERFORM
                   DISPLAY " "
           END-PERFORM
           .